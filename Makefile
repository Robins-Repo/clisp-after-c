INSTALL_PATH = "/usr/local/bin"
LIBRARY_PATH = "/usr/lib/CLick"
INCLUDE_PATH = "/usr/include/CLick"

DEBUG =
LINK =
FLAGS = $(DEBUG) $(LINK)
COMPILER = c++

default:debug

debug: DEBUG = -g3
debug: structure
	$(COMPILER) -o ./build/Zoom Zoom.cpp $(FLAGS)
	$(COMPILER) -o ./build/Z Z.cpp $(FLAGS)
	$(COMPILER) -o ./build/CL CL.cpp $(FLAGS)
	chmod u+x ./build/CL ./build/Z ./build/Zoom

release:
	mkdir -p release
	$(COMPILER) -o ./release/Zoom Zoom.cpp $(FLAGS)
	$(COMPILER) -o ./release/Z Z.cpp $(FLAGS)
	$(COMPILER) -o ./release/CL CL.cpp $(FLAGS)

structure:
	mkdir -p build

install:
	mkdir -p INSTALL_PATH
	mkdir -p LIBRARY_PATH
	mkdir -p INCLUDE_PATH
	chmod a+x ./release/Zoom ./release/Z ./release./CL
	cp ./release/Zoom ./release/Z ./release/CL $(INSTALL_PATH)

test1:
	./build/debug/Zoom

test2:
	./build/debug/Z
test3:
	./build/debug/CL
