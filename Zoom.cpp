/*
* This code is Copyright (c) Daniel (Robin) Smith
* Author: Daniel (Robin( Smith <smithtrombone@gmail.com> 2020 - Present
*
* See README for more information about this project.
* See LICENSE for more information on use and distribution of this code.
*/

//Zoom expects the terminal to be mostly ECMA-48 compliant

////////////////////////////////////////////////////////////////////////////////
//	INCLUDES
////////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <cstdlib>
#include <stdio.h>
#include <stdio_ext.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <pwd.h>
#include <time.h>
#include <string>
#include <dirent.h>
#include <exception>
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	GLOBAL DECLARATIONS AND CONSTANTS
////////////////////////////////////////////////////////////////////////////////
//	STRING CONSTANTS========================================================
const char
//contains version number
*VERSION = {"0.1.0 Pre-Alpha"},
//prints command line options for Zoom
*HELP {"Zoom [?vif] (forms)\n?\tPrints this message.\nv\tPrints the \
version number.\ni\tPrints instructions for the Zoom interface.\nf\tAllows an \
arbitrary number of Lisp forms to be loaded into the REPL on initialization.\n\
forms\tThe Lisp forms being added after option\"f\"\n"},
//prints the Zoom interface key bindings to the command line
*BINDINGS {"The Zoom Interface\n\tZoom has two vertically stacked windows \
that present a Lisp REPL on top of a text entry area. The user interacts with \
the bottom window to enter Lisp forms into the REPL. The output from the REPL \
will be drawn in the top window.\n\tWhen Zoom starts, the user can type on the \
keyboard to enter text into the bottom window. The user can access various \
additional functions by combining letters with the Ctrl key. Zoom has the \
following key bindings:\n\
Ctrl + q\tExits Zoom.\n\
Arrow Keys\tMoves the cursor around the window.\n\
Ctrl + i\tToggles the text input mode between Overtype (default) and Insert.\n\
Ctrl + c\tSends the input window to the REPL, but doesn't clear the window.\n\
Ctrl + d\tSends the input window to the REPL, and clears the window.\n\
Ctrl + w\tToggles hiding the text editor to use Zoom as a traditional REPL shell\n\
Ctrl + l\tScrolls the output window downward.\n\
Ctrl + k\tScrolls the output window upward.\n\
Ctrl + z\tIterates through previous input forms.\n\
Ctrl + x\tIterates through autocomplete suggestions.\n\
Ctrl + s\tExports the current text in the input window to a text file.\n\
Ctrl + e\tLoads a text file into the input window buffer.\n\
Ctrl + o\tImports a text file into the input window.\n\
Ctrl + a\tToggles colors on and off\n\
CTRL + r\tRefreshes the screen\n"},
*CONFORMANCE_STATEMENT {"Zoom conforms with the requirements of ANSI X3.226."};
//      KEYCODE CONSTANTS=======================================================
#define CTRL_Q 17
#define CTRL_W 23
#define CTRL_S 19
#define CTRL_A 1
#define CTRL_D 4
#define CTRL_O 15
#define CTRL_C 3
#define CTRL_E 5
#define CTRL_N 14
#define CTRL_L 12
#define CTRL_K 11
#define CTRL_Z 26
#define CTRL_X 24
#define CTRL_P 16
#define CTRL_R 18
#define CUR_UP -1
#define CUR_DOWN -2
#define CUR_LEFT -3
#define CUR_RIGHT -4
//      ANSI ESCAPE CODES=======================================================
#define NEXT_LINE "\r\n"
#define CLEAR_LINE "\x1b[K"
#define CLEAR_SCREEN "\x1b[2J"
#define SETC_I "\x1b[?25l"
#define SETC_V "\x1b[?25h"
#define SETC_TOPL "\x1b[1;1H"
#define UNDERLINE "\x1b[4m"
#define NO_STYLE "\x1b[0m"
#define BOLD "\x1b[1m"
//      TAB LENGTH==============================================================
#define TABL 8
//      TERMINFO PATHS==========================================================
const char
*TERMINFO_DIR[2] = {"/usr/share/terminfo/", "/usr/lib/terminfo/"};
//      TRUECOLOR TERMINAL ESCAPE CODES=========================================
//Here's a list of Solarized colors to choose from for true color terminals
//base03: 0 43 54		| dark blue
//base02: 7 54 66		| navy blue
//base01: 88 110 117		| dark gray that matches 03 and 02
//base00: 101 123 131		| light gray that matches 03 and 02
//base0: 131 148 150		| light gray that matches 3 and 2
//base1: 147 161 161		| dark gray that matches 3 and 2
//base2: 238 232 213		| tan
//base3: 253 246 227		| egshell white
//yellow: 181 137 0
//orange: 203 75 22
//red 220 50 47
//magenta: 211 54 130
//violet: 108 113 196
//blue: 38 139 210
//cyan 42 161 152
//green 133 153 0
//======BASE EDITOR AND DISPLAY COLORS==========================================
//background for output window
#define BG1 "\x1b[48;2;0;43;54m"
//background for input window
#define BG2 "\x1b[48;2;238;232;213m"
//foreground for output window
#define FG1 "\x1b[38;2;147;161;161m"
//foreground for input window
#define FG2 "\x1b[38;2;131;148;150m"
//background for cursor
#define BGC "\x1b[48;2;253;246;227m"
//color for line numbers
#define LN "\x1b[38;2;42;161;m"
//======SYNTAX HIGHLIGHTING COLORS==============================================
//parentheses, "'", "[]", and "|"
#define TERMINATING "\x1b[38;2;133;153;0m"
//"\"
#define ESCAPE "\x1b[38;2;203;75;22;m"
#define STRINGS ""
#define SYMBOLS
/*
HL1 = "\x1b[38;2;211;54;138m",
//highlight 2 is for symbols and quotes
*HL2 = "\x1b[38;2;203;75;22m",
//highlight 3 is for function names
*HL3 = "\x1b[38;2;42;161;152m",
//highlight 4 is for parentheses
*HL4 = "\x1b[38;2;133;153;0m";
*/
//      ERROR STRINGS ==========================================================
const char
*ERR_end_of_file = {"End of File encountered too early!"},
*ERR_reader_error_invalid = {"The Reader encountered an invalid character!"},
*ERR_right_p = {"The Reader encountered an unmatched right parenthesis"};
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	THE ZOOM VIRTUAL MACHINE
////////////////////////////////////////////////////////////////////////////////

//linked list node for the virtual memory page =================================
struct addr
{
	char c;
	addr *a, *b;
};
//virtual CPU registers ========================================================
long long int reg[16];
long double freg[8];

//head and tail of virtual memory page =========================================
addr *a, *z;

/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	VARIABLES FOR RUNNING THE REPL
////////////////////////////////////////////////////////////////////////////////

//string that represents the standard readtable ================================
//the last character represents the readtable case
//a = :upcase, b = :downcase, c = :preserve, d = :invert
const char* STANDARD_READTABLE = "       \a\b\t\n\v\f\r                   !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ a";
std::string READTABLE;
//readtable case flag ==========================================================
//0 = short-float, 1 = single-float, 2 = double-float, 3 = long-float
int READ_DEFAULT_FLOAT_FORMAT = 1;
bool READ_SUPPRESS;
int READ_BASE = 10;

//current readtable ============================================================
std::string crt;

/*
language hierarchy:
1: Environment Objects - struct env
2: Packages - struct package
3: Symbols - struct symbol
4: System Classes

Types are signified by the 't' variable in the symbol and atom structs.
't' in the atom struct signifies the object type pointed to by 'a'.
't' can hold:
0 to signify an atom,
1 to signify a symbol,
2 to signify the quote special operator.
In the symbol struct, 't' signifies the data type.
1...
*/
struct env;
struct package;
struct symbol;
struct object;
struct property;
struct sys_class
{
	std::string name;
	std::vector<std::string>supertypes;
};
struct object
{
	sys_class *t;
	addr *data;
};
struct property
{
	object indicator, value;
};
struct symbol
{
	std::string name;
	package *p;
	object *value;
	bool function;
	std::vector<property>properties;
	bool shadowed;
	int t = 1;
};
struct package
{
	std::string name;
	std::vector<std::string> nickname;
	std::vector<symbol*> internal;
	std::vector<symbol*> external;
};
struct env
{
	int cPackage = 2; //the position of the current package.
	std::vector<package>p; //list of all existing packages
	std::vector<sys_class>c; //list of all existing system classes/types
};
env GLOBAL_ENV;
struct atom
{
	atom *p, *b = nullptr; //pointer to parent atom and the right branch
	void *a = nullptr; //left branch
	int t = 0; //type indicator for the left branch
};

//function to execute the REPL ================================
void RUN (std::string input);

//function to perform autocomplete suggestions =================================
std::string MATCH (std::string pattern);

//function that performs the read algorithm ====================================
void read (atom *a, std::string &s, int &pos, int &parens);
//reads a token (step 8 of the reader algorithm)
bool getToken (std::string &s, int &pos, std::string &t);
//reads a token (step 9 of the reader algorithm)
bool getTokenE (std::string &s, int &pos, std::string &t);
void eval (atom *a);
void errorOutput (const char *s);
void debugOutput (char *s);

/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	THE EDITOR
////////////////////////////////////////////////////////////////////////////////

termios editor, preserve;
std::vector<std::string> OBUFFER, IBUFFER;
std::vector<std::vector<std::string>>previousInput;
int previousInScroll = 0;
std::string shellInput = "";
std::vector<std::string>prevShellInput;
int prevShellScroll = 0;
bool isInit = true;
bool resized = false;
bool overtype = true;
bool shell = true;
bool colored = false;
int cx = 0, cy = 0, shellC = 0, inputH, outputH, ISC = 0, OSC = 0, HISC = 0, HOSC = 0;
winsize WIN;
void getWinSize ();
void setScreen (int s);
void setScreen ();
void handleKey ();
void softPush ();
void hardPush ();
void restore ();
void hardExit (const char *s);
void drawCur ();

/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	ENTRY POINT (Main)
////////////////////////////////////////////////////////////////////////////////

int main (int argN, char **argC)
{
	std::string TERM;
	int *termDir, *termFile;
	char mode = '\0';
	int ret;
	FILE *f;
	termios editor;
	std::vector <std::string> forms;
	std::string tmp;
	if (argN > 1)
	{
		mode = argC[2][0];
		switch (mode)
		{
			case 'v':
			{
				std::cout << VERSION << std::endl;
				return 0;
			}
			case '?':
			{
				std::cout << HELP << std::endl;
				return 0;
			}
			case 'i':
			{
				std::cout << BINDINGS << std::endl;
				return 0;
			}
			case 'f':
			{
				for (int i = 0; i < argN - 2; i ++)
				{
					forms.push_back (std::string (argC[i]));
				}
			}
			case '\0':
			{
				break;
			}
			default:
			{
				std::cout << HELP << std::endl;
				return 0;
			}
		}
	}
//      Set up "raw mode" ======================================================
	READTABLE.assign (STANDARD_READTABLE);
	tcgetattr (STDIN_FILENO, &preserve);
	editor = preserve;
	editor.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	editor.c_oflag &= ~OCRNL;
	editor.c_oflag |= OPOST;
        editor.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
        editor.c_cflag |= CS8;
        editor.c_cc[VMIN] = 0;
        editor.c_cc[VTIME] = 0;
	if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &editor) == -1)
		hardExit ("tcsetattr failed");

//      hide terminal cursor ===================================================
	write (STDOUT_FILENO, SETC_I, sizeof (SETC_I));

//      Initialize Common Lisp environment defaults ============================
//This is the strangest way to do this but it makes the source more readable.
	//c contains all the standard system class symbols
	GLOBAL_ENV.c = std::vector<sys_class> {
		sys_class {
			std::string {"t"},
			std::vector<std::string> {"t"}
		},
		sys_class {
			std::string {"function"},
			std::vector<std::string> {"t"}
		},
		sys_class {
			std::string {"simple-string"},
			std::vector<std::string> {
				"simple-string",
				"string",
				"vector",
				"simple-array",
				"array",
				"sequence",
				"t"}
		}
	};
	//p contains all the standard packages
	GLOBAL_ENV.p = std::vector<package> {
		package {
			std::string {"KEYWORD"},
			std::vector<std::string>{},
			std::vector<symbol*>{},
			std::vector<symbol*>{}
		},
		package {
			std::string {"COMMON-LISP"},
			std::vector<std::string>{"CL"},
			std::vector<symbol*>{},
			std::vector<symbol*>{}
		},
		package {
			std::string {"COMMON-LISP-USER"},
			std::vector<std::string>{"CL"},
			std::vector<symbol*>{},
			std::vector<symbol*>{}
		}
	};
//      create wait time for top level loop ====================================
	const struct timespec waitPeriod [] {0, 1000000L};
//      Initialize the top level loop for the REPL =============================
	try
	{
		while (1)
		{
			getWinSize ();
			if (resized)
				setScreen ();
			handleKey ();
			nanosleep (waitPeriod, NULL);
		}
	}
	catch (std::exception &t)
	{
		write (STDOUT_FILENO, "\x1b[0m", 4);
	        write (STDOUT_FILENO, "\x1b[2J", 4);
	        write (STDOUT_FILENO, "\x1b[H", 3);
	        write (STDOUT_FILENO, SETC_V, 6);
		tcsetattr (STDOUT_FILENO, TCSAFLUSH, &preserve);
		std::cout << "There was an unexpected error!\n";
		std::cout << t.what () << std::endl;
	}
}

/******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
//	getWinSize
////////////////////////////////////////////////////////////////////////////////
void getWinSize ()
{
	int oldw = WIN.ws_col, oldh = WIN.ws_row;
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &WIN) == -1 or WIN.ws_col == 0)
		hardExit ("Couldn't get window size!");
	else
	{
		if (oldw != WIN.ws_col or oldh != WIN.ws_row)
		{
			resized = true;
			outputH = WIN.ws_row / 4;
			inputH = WIN.ws_row - outputH;
		}
		else
		{
			resized = false;
		}
	}
}
/******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
//	setScreen
////////////////////////////////////////////////////////////////////////////////
void setScreen ()
{
	int o = 0, nl = 0;
	std::string tmp, check;
	if (WIN.ws_col < 32 or WIN.ws_row < 4)
	{
		hardExit ("Screen is too small!\nMinimum screen size is 4x32 characters.");
	}
	if (isInit)
	{
		OBUFFER.resize (4);
		OBUFFER.at(0).assign (";Welcome to Zoom!");
		OBUFFER.at(1).assign (";Copyright (c) 2020");
		OBUFFER.at(2).assign (";Daniel (Robin) Smith");
		OBUFFER.at(3).assign (";");
		OBUFFER.at(3).append (CONFORMANCE_STATEMENT);
		IBUFFER.resize (1);
		isInit = false;
	}
	if (shell)
	{
		tmp.clear ();
		tmp.append (NO_STYLE);
		tmp.append (SETC_TOPL);
		if (colored)
		{
			tmp.append (BG1);
			tmp.append (FG1);
		}
		for (int i = OSC; i < WIN.ws_row + OSC - o - 1; i ++)
		{
			tmp.append (CLEAR_LINE);
			if (i < OBUFFER.size ())
			{
				tmp.append (OBUFFER.at(i));
				tmp.append ("\n");
				if (OBUFFER.at(i).size () > WIN.ws_col)
				{
					o += OBUFFER.at(i).size () / WIN.ws_col;
				}
			}
			else
			{
				if (i < WIN.ws_row + OSC - o - 1)
					tmp.append ("\n");
			}
		}
		if (colored)
		{
			tmp.append (BG2);
			tmp.append (FG2);
		}
		tmp.append (CLEAR_LINE);
		if (shellInput.size () > 0)
		{
			if (shellC < shellInput.size ())
			{
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (">");
				tmp.append (shellInput.substr (0, shellC));
				if (colored)
				{
					tmp.append (LN);
					tmp.append (BGC);
				}
				tmp.append (UNDERLINE);
				tmp.push_back (shellInput.at (shellC));
				tmp.append (NO_STYLE);
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (shellInput.substr (shellC + 1, std::string::npos));
			}
			else
			{
				if (colored)
				{
					tmp.append (BG2);
					tmp.append (FG2);
				}
				tmp.append (">");
				tmp.append (shellInput);
				if (colored)
				{
					tmp.append (LN);
					tmp.append (BGC);
				}
				tmp.append (UNDERLINE);
				tmp.append (" ");
				tmp.append (NO_STYLE);
				if (colored)
				{
					tmp.append (BG1);
					tmp.append (FG1);
				}
			}
		}
		else
		{
			if (colored)
			{
				tmp.append (BG2);
				tmp.append (FG2);
			}
			tmp.append (">");
			if (colored)
				tmp.append (LN);
			tmp.append (UNDERLINE);
			tmp.append (" ");
			tmp.append (NO_STYLE);
			if (colored)
			{
				tmp.append (BG1);
				tmp.append (FG1);
			}
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
	}
	else
	{
//======print the output window text============================================
		tmp.clear ();
		tmp.append (NO_STYLE);
		tmp.append (SETC_TOPL);
		if (colored)
		{
			tmp.append (BG1);
			tmp.append (FG1);
		}
		for (int i = OSC; i < outputH + OSC - o; i ++)
		{
			tmp.append (CLEAR_LINE);
			if (i < OBUFFER.size ())
			{
				tmp.append (OBUFFER.at(i));
				tmp.append ("\n");
				if ((OBUFFER.at(i).size () / WIN.ws_col) > 0)
					o += OBUFFER.at(i).size () / WIN.ws_col;
			}
			else
			{
				if (i < outputH + OSC - o)
					tmp.append ("\n");
			}
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
//======print the input window==================================================
//======make some variables for accounting for tab expansions===================
//off keeps track of how many characters the tabs expand the string to
		int off, len, i;
//isY is true if the current line being processed has the cursor in it
		bool isY;
//======clear output buffer string==============================================

		tmp.clear ();
		tmp.append ("\x1b[");
		tmp.append (std::to_string (outputH + 1));
		tmp.append (";1H");
		if (colored)
		{
			tmp.append (BG2);
			tmp.append (FG2);
		}
		for (i = 0; i < inputH - 1; i ++)
		{
			tmp.append (CLEAR_LINE);
			tmp.append ("\n");
		}
		tmp.append ("\x1b[");
		tmp.append (std::to_string (outputH + 1));
		tmp.append (";1H");
		len = 0;
		i = ISC;
//======loop through lines of input=============================================
		for (int h = ISC; h < inputH + ISC - len; h ++)
		{
			tmp.append (CLEAR_LINE);
//======determine if this line has the cursor===================================
			if (cy == i)
				isY = true;
			else
				isY = false;
//======if there is a line at this height, process it to the buffer=============
			if (i < IBUFFER.size ())
			{
//======add line marker=========================================================
				off = 0;
				for (int j = 0; j < IBUFFER.at(i).size (); j ++)
				{
					if (IBUFFER.at(i).at(j) == '\t')
						off += ((j + off) % TABL);
				}
//======subtract number of lines left in loop for every line over 1 that this
//======line has
				len += (IBUFFER.at(i).size () + off + 3) / WIN.ws_col;
				if (isY)
				{
					if (cx < IBUFFER.at(i).size ())
					{
						tmp.append (IBUFFER.at(i).substr (0, cx));
						if (colored)
						{
							tmp.append (BGC);
							tmp.append (LN);
						}
						tmp.append (UNDERLINE);
						tmp.push_back (IBUFFER.at(i).at(cx));
						tmp.append (NO_STYLE);
						if (colored)
						{
							tmp.append (BG2);
							tmp.append (FG2);
						}
						tmp.append (IBUFFER.at(i).substr (cx + 1, std::string::npos));
					}
					else
					{
						tmp.append (IBUFFER.at(i));
						if (colored)
						{
							tmp.append (BGC);
							tmp.append (LN);
						}
						tmp.append (UNDERLINE);
						tmp.append (" ");
						tmp.append (NO_STYLE);
						if (colored)
						{
							tmp.append (BG2);
							tmp.append (FG2);
						}
					}
				}
				else
				{
					tmp.append (IBUFFER.at(i));
				}
				if (i < inputH + ISC - len - 1)
					tmp.append ("\n");
			}
			else
				if (i < inputH + ISC - len - 1)
					tmp.append ("\n");
			i++;
		}
		write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
	}
}

/******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
//	handleKey
////////////////////////////////////////////////////////////////////////////////
void handleKey ()
{
	bool isKey = false;
	char key[7] = {0, 0, 0, 0, 0, 0, 0};
	if (read (STDIN_FILENO, key, 7) == -1 and errno != EAGAIN)
		hardExit ("read error!");
	if (key[0] == 0)
	{
		return;
	}
//======variables for all cases defined here to avoid cross initialization======
	char a[1];
	std::string save, tmp;
	bool err = false;
	std::ifstream in;
//======if an escape sequence was entered=======================================
	if (key[0] == 27)
	{
		switch (key[2])
		{
//======Up arrow key============================================================
			case 'A':
			{
				if (shell)
				{
					if (prevShellInput.size () > 0)
					{
						if (prevShellScroll + 1 < prevShellInput.size ())
							prevShellScroll ++;
						shellInput.assign (prevShellInput.at(prevShellScroll));
					}
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy - ISC == 0)
					{
						if (ISC > 0)
							ISC --;
						if (cy > 0)
							cy --;
					}
					else
						if (cy > 0)
							cy --;
					if (cx > IBUFFER.at(cy).size ())
					{
						cx = IBUFFER.at(cy).size ();
						if (cx > WIN.ws_col - 1)
						{
							HISC = cx - WIN.ws_col - 1;
						}
					}
				}
				setScreen ();
				break;
			}
//======Down arrow key==========================================================
			case 'B':
			{
				if (shell)
				{
					if (prevShellScroll > 0)
					{
						prevShellScroll --;
						if (prevShellScroll == 0)
						{
							shellInput.clear ();
							shellC = 0;
						}
						else
							shellInput.assign (prevShellInput.at(prevShellScroll));
					}
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy - ISC == inputH - 1)
					{
						if (cy - ISC > 0)
							ISC ++;
						if (cy < IBUFFER.size () - 1)
							cy ++;
					}
					else
					{
						if (cy < IBUFFER.size () - 1)
							cy ++;
					}
					if (cx > IBUFFER.at(cy).size ())
					{
						cx = IBUFFER.at(cy).size ();
						if (cx > WIN.ws_col - 1)
						{
							HISC = cx - WIN.ws_col - 1;
						}
					}
				}
				setScreen ();
				break;
			}
//======Right arrow key=========================================================
			case 'C':
			{
				if (shell)
				{
					if (shellC < shellInput.size ())
						shellC ++;
				}
				else
				{
					if (IBUFFER.size () == 0)
						break;
					if (cy < IBUFFER.size ())
					{
						if (cx < IBUFFER.at(cy).size ())
						{
							cx ++;
							if (cx > WIN.ws_col - 1)
								HISC ++;
						}
					}
				}
				setScreen ();
				break;
			}
//======Left arrow key==========================================================
			case 'D':
			{
				if (shell)
				{
					if (shellC > 0)
						shellC --;
				}
				else
				{
					if (cy < IBUFFER.size ())
					{
						if (cx > 0)
						{
							if (HISC > 0)
								HISC --;
							cx --;
						}
					}
				}
				setScreen ();
				break;
			}
		}
		return;
	}
	switch (key[0])
	{
//======quits zoom==============================================================
		case CTRL_Q:
		{
			restore ();
			break;
		}
//======hard push to the REPL, clearing the input window========================
		case CTRL_D:
		{
			if (shell)
				restore();
			std::string tmp;
			for (int i = 0; i < IBUFFER.size (); i++)
			{
				OBUFFER.push_back (";Z: ");
				OBUFFER.back().append(IBUFFER.at(i));
				tmp.append (IBUFFER.at(i));
				tmp.append ("\n");
			}
			RUN (tmp);
			previousInput.insert (previousInput.begin (), IBUFFER);
			IBUFFER.clear ();
			cx = 0;
			cy = 0;
			ISC = 0;
			if (OBUFFER.size () > outputH)
			{
				OSC = OBUFFER.size () - outputH;
			}
			setScreen ();
			break;
		}
//======toggles entry modes between overtype and insert
		case CTRL_O:
		{
			overtype = !overtype;
			break;
		}
//======soft push to the REPL, doesn't clear the input window===================
		case CTRL_C:
		{
			if (shell)
				break;
			std::string tmp;
			for (int i = 0; i < IBUFFER.size (); i ++)
			{
				OBUFFER.push_back (";Z: ");
				OBUFFER.back().append (IBUFFER.at(i));
				tmp.append (IBUFFER.at(i));
				tmp.append ("\n");
			}
			RUN (tmp);
			if (OBUFFER.size () > outputH)
			{
				OSC = OBUFFER.size () - outputH;
			}
			setScreen ();
			break;
		}
		case CTRL_N:
		{
			break;
		}
		case CTRL_W:
		{
			if (OBUFFER.size () > WIN.ws_row)
			{
				OSC = OBUFFER.size () - WIN.ws_row;
			}
			shell = !shell;
			setScreen ();
			break;
		}
		case CTRL_A:
		{
			colored = !colored;
			setScreen ();
			break;
		}
		case CTRL_K:
		{
			if (OSC < OBUFFER.size () - 1)
			{
				OSC ++;
				setScreen ();
			}
			break;
		}
		case CTRL_L:
		{
			if (OSC > 0)
			{
				OSC --;
				setScreen ();
			}
			break;
		}
		case CTRL_Z:
		{
			if (previousInput.size () == 0)
				break;
			if (previousInScroll >= previousInput.size ())
				break;
			else
			{
				if (previousInScroll == 0)
				{
					previousInput.insert (previousInput.begin (), IBUFFER);
					previousInScroll ++;
				}
				IBUFFER = previousInput.at(previousInScroll);
				if (previousInScroll < previousInput.size ())
					previousInScroll ++;
				cx = 0;
				cy = 0;
				ISC = 0;
				HISC = 0;
				setScreen ();
			}
			break;
		}
		case CTRL_X:
		{
			if (previousInScroll > 0)
			{
				previousInScroll --;
				IBUFFER = previousInput.at(previousInScroll);
				cx = 0;
				cy = 0;
				ISC = 0;
				HISC = 0;
				setScreen ();
			}
			break;
		}
		case CTRL_E:
		{
			previousInput.push_back (IBUFFER);
			tmp.append (NO_STYLE);
			tmp.append (SETC_TOPL);
			tmp.append (CLEAR_SCREEN);
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			tmp.assign(SETC_TOPL);
			tmp.append ("Please enter the path/name of the lisp form you want to load.\n");
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			save.clear ();
			while (1)
			{
				a[0] = 0;
				if (read (STDIN_FILENO, a, 1) == -1 and errno != EAGAIN)
					hardExit ("read error!");
				if (a[0] == 0)
				{
					const struct timespec waitPeriod [] {0, 1000000L};
					nanosleep (waitPeriod, NULL);
					continue;
				}
				if (a[0] == 13 or a[0] == 10)
					break;
				if (a[0] == 127 or a[0] == 10)
				{
					write (STDOUT_FILENO, "\r", 1);
					write (STDOUT_FILENO, CLEAR_LINE, sizeof (CLEAR_LINE));
					save.pop_back ();
					write (STDOUT_FILENO, save.c_str (), save.size ());
				}
				if (a[0] < 127 and a[0] > 31)
				{
					save.push_back (a[0]);
					write (STDOUT_FILENO, a, 1);
				}
			}
			FILE *in;
			in = fopen (save.c_str (), "r");
			if (in == 0)
			{
				OBUFFER.push_back ("Z) Error opening lisp form!");
				OBUFFER.push_back (save);
			}
			else
			{
				OBUFFER.push_back ("Z) Loading lisp form.");
				OBUFFER.push_back (save);
				IBUFFER.clear ();
				IBUFFER.emplace_back ();
				while (fread (a, 1, 1, in) != 0)
				{
					if (a[0] > 31)
					{
						IBUFFER.back ().push_back (a[0]);
					}
					else
					{
						IBUFFER.emplace_back ();
					}
				}
			}
			cx = 0;
			cy = 0;
			HISC = 0;
			ISC = 0;
			setScreen ();
			break;
		}
		case CTRL_S:
		{
			tmp.append (NO_STYLE);
			tmp.append (SETC_TOPL);
			tmp.append (CLEAR_SCREEN);
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			tmp.assign(SETC_TOPL);
			tmp.append ("Please type the path/name where you want to save your lisp form\n");
			write (STDOUT_FILENO, tmp.c_str (), tmp.size ());
			save.clear ();
			while (1)
			{
				a[0] = 0;
				if (read (STDIN_FILENO, a, 1) == -1 and errno != EAGAIN)
					hardExit ("read error!");
				if (a[0] == 0)
				{
					const struct timespec waitPeriod [] {0, 1000000L};
					nanosleep (waitPeriod, NULL);
					continue;
				}
				if (a[0] == 13 or a[0] == 10)
					break;
				if (a[0] == 127 or a[0] == 10)
				{
					write (STDOUT_FILENO, "\r", 1);
					write (STDOUT_FILENO, CLEAR_LINE, sizeof (CLEAR_LINE));
					save.pop_back ();
					write (STDOUT_FILENO, save.c_str (), save.size ());
				}
				if (a[0] < 127 and a[0] > 31)
				{
					save.push_back (a[0]);
					write (STDOUT_FILENO, a, 1);
				}
			}
			FILE *out;
			out = fopen (save.c_str (), "w");
			if (out == 0)
			{
				OBUFFER.push_back ("Error saving lisp form to file!");
				OBUFFER.push_back (save);
			}
			else
			{
				OBUFFER.push_back ("Z) Saving Lisp Form to file:");
				OBUFFER.push_back (save);
				for (int i = 0; i < IBUFFER.size (); i ++)
				{
					if (fwrite (IBUFFER.at(i).c_str (), 1, IBUFFER.at(i).size (), out) == 0)
					{
						OBUFFER.push_back ("Z) Error saving Lisp Form to file!");
						OBUFFER.push_back ("Line ");
						OBUFFER.back ().append (std::to_string (i));
						err = true;
					}
					if (fwrite ("\n", 1, 1, out) == 0)
					{
						OBUFFER.push_back ("Z) Error saving Lisp Form to file!");
						OBUFFER.push_back ("Line ");
						OBUFFER.back ().append (std::to_string (i));
						err = true;
					}
				}
				if (err)
					OBUFFER.push_back ("Z) Lisp Form saved with errors!");
				else
					OBUFFER.push_back ("Z) Lisp Form saved without errors.");
			}
			setScreen ();
			break;
		}
		case CTRL_R:
		{
			getWinSize ();
			setScreen();
			break;
		}
//======Case for new line and carriage return characters========================
		case 13:
		case 10:
		{
			if (shell)
			{
				prevShellInput.insert (prevShellInput.begin (), shellInput);
				prevShellScroll = 0;
				OBUFFER.push_back (";Z: ");
				OBUFFER.back().append (shellInput);
				RUN (shellInput);
				shellInput.clear ();
				shellC = 0;
				if (OBUFFER.size () > WIN.ws_row)
				{
					OSC = OBUFFER.size () - WIN.ws_row;
				}

			}
			else
			{
				previousInScroll = 0;
				if (IBUFFER.size () == 0)
				{
					IBUFFER.resize (2);
					cy = 1;
					cx = 0;
				}
				else
				{
					if (cy + 1 <= IBUFFER.size ())
					{
						if (cx >= IBUFFER.at(cy).size ())
							IBUFFER.emplace (IBUFFER.begin () + cy + 1);
						else
						{
							IBUFFER.insert (IBUFFER.begin () + cy + 1, IBUFFER.at(cy).substr (cx, std::string::npos));
							IBUFFER.at(cy).erase (cx, std::string::npos);
						}
						cy ++;
						cx = 0;
					}
					else
					{
						if (cx >= IBUFFER.at(cy).size ())
							IBUFFER.emplace_back ();
						else
						{
							IBUFFER.push_back (IBUFFER.at(cy).substr (cx, std::string::npos));
							IBUFFER.at(cy).erase (cx, std::string::npos);
						}
						cy ++;
						cx = 0;
					}
				}
				if (cy >= inputH)
					ISC ++;
			}
			setScreen ();
			break;
		}
//======case for ascii delete "backspace" key===================================
		case 8:
		case 127:
		{
			if (shell)
			{
				if (shellInput.size () == 0)
					break;
				if (shellC > 0)
				{
					shellInput.erase (shellC - 1, 1);
					shellC --;
				}
			}
			else
			{
				previousInScroll = 0;
				if (IBUFFER.size () == 0)
					break;
				if (cx + HISC > 0)
				{
					IBUFFER.at(cy).erase (cx - 1, 1);
					if (cx > 0)
						cx --;
					else
					{
						HISC --;
					}
				}
				else
				{
					if (cy != 0)
					{
						IBUFFER.erase (IBUFFER.begin () + cy);
						cy --;
						cx = IBUFFER.at(cy).size ();
					}
				}
			}
			setScreen ();
			break;
		}
		default:
		{
			if (shell)
			{
				if (key[0] < 127 and key[0] > 31)
				{
					prevShellScroll = 0;
					if (shellInput.size () == 0)
					{
						shellInput.clear ();
						shellInput.assign (&key[0]);
						shellC ++;
					}
					else if (shellC == shellInput.size ())
					{
						shellInput.push_back (key[0]);
						shellC ++;
					}
					else
					{
						if (overtype)
						{
							shellInput.at(shellC) = key[0];
							shellC ++;
						}
						else
						{
							shellInput.insert (shellC, &key[0]);
							shellC ++;
						}
					}
				}

			}
			else
			{
				if ((key[0] < 127 and key[0] > 31) or key[0] == 9)
				{
					previousInScroll = 0;
					if (IBUFFER.size () == 0)
					{
						IBUFFER.push_back (&key[0]);
						cx ++;
					}
					else
					{
						if (cx == IBUFFER.at(cy).size ())
						{
							IBUFFER.at(cy).insert (cx, &key[0]);
							cx ++;
						}

						else
						{
							if (overtype)
							{
								IBUFFER.at(cy).at(cx) = key[0];
								cx ++;
							}
							else
							{
								IBUFFER.at(cy).insert (cx, &key[0]);
								cx ++;
							}
						}
					}
				}
			}
			setScreen();
			break;
		}
	}
	return;
}
/******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
//	restore ()
////////////////////////////////////////////////////////////////////////////////

void restore ()
{
	if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &preserve) == -1)
		hardExit ("restore failed!");
	write (STDOUT_FILENO, "\x1b[0m", 4);
	write (STDOUT_FILENO, "\x1b[2J", 4);
	write (STDOUT_FILENO, "\x1b[H", 3);
	write (STDOUT_FILENO, SETC_V, sizeof (SETC_V));
	std::cout << "Thank you for using Zoom!\n";
	exit (0);
}

/******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
//	void hardExit
////////////////////////////////////////////////////////////////////////////////

void hardExit (const char *s)
{
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &preserve);
	write (STDOUT_FILENO, "\x1b[0m", 4);
//clear the screen
        write(STDOUT_FILENO, "\x1b[2J", 4);
//move cursor to left
        write(STDOUT_FILENO, "\x1b[H", 3);
        write (STDOUT_FILENO, SETC_V, 6);
//print error and exit program
        perror (s);
        exit (-1);
}

////////////////////////////////////////////////////////////////////////////////
//	RUN
////////////////////////////////////////////////////////////////////////////////

void RUN (std::string input)
{
	atom *start;
	int pos = 0, parens = 0;
	while (pos < input.size ())
	{
		read (start, input, pos, parens);
		eval (start);
	}
}

////////////////////////////////////////////////////////////////////////////////
//	read
////////////////////////////////////////////////////////////////////////////////

void read (atom *a, std::string &s, int &pos, int &parens)
{
	a = new atom;
	int e = 0;
	std::string token;
//      loop analyzing the input ===============================================
	while (pos < s.size ())
	{
		//get readtable position
		for (int i = 0; i < READTABLE.size () - 1; i ++)
		{
			if (s.at(pos) == READTABLE.at(i))
			{
				e = i;
				break;
			}
		}
//      invalid or whitespace ==================================================
		//invalid characters
		if (e < 8 or e > 13 and e < 31)
		{
			errorOutput(ERR_reader_error_invalid);
			pos ++;
		}
		//whitespace
		else if ((e > 7 and e < 14) or e == 32)
		{
			pos ++;
		}
//      terminating/non-terminating macro characters ===========================
		//double quotes
		else if (e == 34)
		{
			pos ++;
			if (pos >= s.size ())
			{
				errorOutput (ERR_end_of_file);
				return;
			}
			while (1)
			{
				for (int i = 0; i < READTABLE.size () - 1; i ++)
				{
					if (READTABLE.at(i) == s.at(pos))
					{
						e == i;
						break;
					}
				}
				if (e == 92)
				{
					pos ++;
					if (pos >= s.size ())
					{
						errorOutput (ERR_end_of_file);
						return;
					}
					token.push_back(s.at(pos));
				}
				if (e == 34)
				{
					pos ++;
					break;
				}
				token.push_back (READTABLE.at(e));
				pos ++;
				if (pos >= s.size ())
				{
					errorOutput (ERR_end_of_file);
					return;
				}
			}
		}
		//pound sign
		else if (e == 35)
		{
			pos ++;
		}
		//apostrophe/single quote
		else if (e == 39)
		{
			a->t = 2;
			a->a = new atom;
			pos ++;
			if (pos >= s.size ())
			{
				errorOutput (ERR_end_of_file);
				return;
			}
			read ((atom*)a->a, s, pos, parens);
		}
		//open paren
		else if (e == 40)
		{
			pos ++;
			parens ++;
			if (pos >= s.size ())
			{
				errorOutput (ERR_end_of_file);
				return;
			}
			atom *t = new atom;
			t->p = a;
			a->a = t;
			read ((atom*)a->a, s, pos, parens);
		}
		//close paren
		else if (e == 41)
		{
			pos ++;
			if (parens == 0)
			{
				errorOutput (ERR_right_p);
				return;
			}
			parens --;
			return;
		}
		//comma
		else if (e == 44)
		{
			pos ++;
		}
		//semicolon
		else if (e == 59)
		{
			while (e != 10)
			{
				pos ++;
				for (int i = 0; i < READTABLE.size () - 1; i ++)
				{
					if (READTABLE.at(i) == s.at(pos))
						e == i;
				}
			}
			pos ++;
		}
		//backquote
		else if (e == 96)
		{
			pos ++;
		}
//      single escape character (backslash) ====================================
		else if (e == 92)
		{
			pos ++;
			if (pos >= s.size ())
			{
				errorOutput (ERR_end_of_file);
				return;
			}
			if (!getToken (s, pos, token))
				return;
		}
//      multiple escape character (pipe) =======================================
		else if (e == 124)
		{
			pos ++;
			if (pos >= s.size ())
			{
				errorOutput (ERR_end_of_file);
				return;
			}
			if (!getTokenE (s, pos, token))
				return;
		}
//      constituent characters =================================================
		else
		{
			token.push_back (READTABLE.at(e));
			pos ++;
			if (!getToken (s, pos, token))
				return;
		}
	}
//      check if the token is a number =========================================
	//loop for length of token]
	switch (READTABLE.back ())
	{
		case 'a':
		{
			for (int i = 0; i < token.size (); i ++)
			{
				for (int j = 0; j < READTABLE.size () - 1; j ++)
				{
					if (token.at(i) == READTABLE.at(j))
					{
						e = j;
						break;
					}
				}

			}
			break;
		}
		case 'b':
		{
			for (int i = 0; i < token.size (); i ++)
			{

			}
			break;
		}
		case 'c':
		{
			for (int i = 0; i < token.size (); i ++)
			{

			}
			break;
		}
		case 'd':
		{
			for (int i = 0; i < token.size (); i ++)
			{

			}
			break;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
//	getToken
////////////////////////////////////////////////////////////////////////////////
bool getToken (std::string &s, int &pos, std::string &t)
{
	int e;
	while (pos < s.size())
	{
		for (int i = 0; i < READTABLE.size () - 1; i ++)
			if (s.at(pos) == READTABLE.at(i))
			{
				e = i;
				break;
			}
		if (e < 8 or e > 13 and e < 31)
		{
			errorOutput(ERR_reader_error_invalid);
			return false;
		}
		else if ((e > 7 and e < 14) or e == 32)
		{
			break;
		}
		else if (e == 34 or e == 39 or e == 40 or e == 41 or e == 44 or e == 59 or e == 96 or e == 92 or e == 124)
		{
			break;
		}
		else if (e == 92)
		{
			t.push_back (READTABLE.at(e));
			pos ++;
		}
		else if (e == 124)
		{
			pos ++;
			getTokenE (s, pos, t);
		}
		else
		{
			if (READTABLE.back () == 'a')
			{
				if (e > 96 and e < 123)
					t.push_back (READTABLE.at(e - 32));
				else
					t.push_back (READTABLE.at(e));
			}
			else if (READTABLE.back () == 'b')
			{
				if (e > 64 and e < 91)
					t.push_back (READTABLE.at(e + 32));
				else
					t.push_back (READTABLE.at(e));
			}
			else if (READTABLE.back () == 'c')
			{
				t.push_back (READTABLE.at(e));
			}
			pos ++;
		}
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
//	getTokenE
////////////////////////////////////////////////////////////////////////////////
bool getTokenE (std::string &s, int &pos, std::string &t)
{
	int e;
	while (pos < s.size ())
	{
		for (int i = 0; i < READTABLE.size () - 1; i ++)
		{
			if (s.at(pos) == READTABLE.at(i))
			{
				e = i;
				break;
			}
		}
		if (e < 8 or (e > 13 and e < 31))
		{
			errorOutput (ERR_reader_error_invalid);
			return false;
		}
		else if (e == 124)
		{
			pos ++;
			getToken (s, pos, t);
		}
		else if (e == 92)
		{
			pos ++;
			if (pos < s.size ())
				t.push_back (READTABLE.at(e));
			else
			{
				errorOutput ("The Reader encountered an invalid character");
				return false;
			}
		}
		else
		{
			t.push_back (READTABLE.at(e));
			pos ++;
		}
	}
	errorOutput ("The Reader encountered the end of the file too soon!");
	return false;
}

////////////////////////////////////////////////////////////////////////////////
//	eval
////////////////////////////////////////////////////////////////////////////////
void eval (atom *a)
{
}
////////////////////////////////////////////////////////////////////////////////
//	errorOutput
////////////////////////////////////////////////////////////////////////////////
void errorOutput (const char *s)
{
	OBUFFER.push_back (s);
}
