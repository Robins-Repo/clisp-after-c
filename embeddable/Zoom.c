/*
* This code is Copyright (c) Daniel (Robin) Smith
* Author: Daniel (Robin( Smith <smithtrombone@gmail.com> 2020 - Present
*
* See README for more information about this project.
* See LICENSE for more information on use and distribution of this code.
*/

//Zoom expects the terminal to be mostly ECMA-48 compliant

////////////////////////////////////////////////////////////////////////////////
//	INCLUDES
////////////////////////////////////////////////////////////////////////////////
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <stdlib.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <limits.h>
#include <string.h>
#include <pwd.h>
#include <time.h>
#include <dirent.h>
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	GLOBAL DECLARATIONS AND CONSTANTS
////////////////////////////////////////////////////////////////////////////////
//	STRINGS ================================================================
char
//contains version number
*VERSION = {"0.1.0 Pre-Alpha\0"},
//prints command line options for Zoom
*HELP =  {"Zoom [?vif] (forms)\n?\tPrints this message.\nv\tPrints the \
version number.\ni\tPrints instructions for the Zoom interface.\nf\tAllows an \
arbitrary number of Lisp forms to be loaded into the REPL on initialization.\n\
forms\tThe Lisp forms being added after option\"f\"\n\0"},
//prints the Zoom interface key bindings to the command line
*BINDINGS = {"The Zoom Interface\n\tZoom has two vertically stacked windows \
that present a Lisp REPL on top of a text entry area. The user interacts with \
the bottom window to enter Lisp forms into the REPL. The output from the REPL \
will be drawn in the top window.\n\tWhen Zoom starts, the user can type on the \
keyboard to enter text into the bottom window. The user can access various \
additional functions by combining letters with the Ctrl key. Zoom has the \
following key bindings:\n\
Ctrl + q\tExits Zoom.\n\
Arrow Keys\tMoves the cursor around the window.\n\
Ctrl + i\tToggles the text input mode between Overtype (default) and Insert.\n\
Ctrl + c\tSends the input window to the REPL, but doesn't clear the window.\n\
Ctrl + d\tSends the input window to the REPL, and clears the window.\n\
Ctrl + w\tToggles hiding the text editor to use Zoom as a traditional REPL shell\n\
Ctrl + l\tScrolls the output window downward.\n\
Ctrl + k\tScrolls the output window upward.\n\
Ctrl + z\tIterates through previous input forms.\n\
Ctrl + x\tIterates through autocomplete suggestions.\n\
Ctrl + s\tExports the current text in the input window to a text file.\n\
Ctrl + e\tLoads a text file into the input window buffer.\n\
Ctrl + o\tImports a text file into the input window.\n\
Ctrl + a\tToggles colors on and off\n\
CTRL + r\tRefreshes the screen\n\0"},
*CONFORMANCE_STATEMENT =  {";Zoom conforms with the requirements of ANSI X3.226.\0"};
//      KEYCODE CONSTANTS=======================================================
#define CTRL_Q 17
#define CTRL_W 23
#define CTRL_S 19
#define CTRL_A 1
#define CTRL_D 4
#define CTRL_O 15
#define CTRL_C 3
#define CTRL_E 5
#define CTRL_N 14
#define CTRL_L 12
#define CTRL_K 11
#define CTRL_Z 26
#define CTRL_X 24
#define CTRL_P 16
#define CTRL_R 18
#define CUR_UP -1
#define CUR_DOWN -2
#define CUR_LEFT -3
#define CUR_RIGHT -4
//      ANSI ESCAPE CODES=======================================================
#define NEXT_LINE "\r\n"
#define CLEAR_LINE "\x1b[K"
#define CLEAR_SCREEN "\x1b[2J"
#define SETC_I "\x1b[?25l"
#define SETC_V "\x1b[?25h"
#define SETC_TOPL "\x1b[1;1H"
#define UNDERLINE "\x1b[4m"
#define NO_STYLE "\x1b[0m"
#define BOLD "\x1b[1m"
//      TAB LENGTH==============================================================
#define TABL 8
//      TERMINFO PATHS==========================================================
const char
*TERMINFO_DIR[2] = {"/usr/share/terminfo/", "/usr/lib/terminfo/"};
//      TRUECOLOR TERMINAL ESCAPE CODES=========================================
//Here's a list of Solarized colors to choose from for true color terminals
//base03: 0 43 54		| dark blue
//base02: 7 54 66		| navy blue
//base01: 88 110 117		| dark gray that matches 03 and 02
//base00: 101 123 131		| light gray that matches 03 and 02
//base0: 131 148 150		| light gray that matches 3 and 2
//base1: 147 161 161		| dark gray that matches 3 and 2
//base2: 238 232 213		| tan
//base3: 253 246 227		| egshell white
//yellow: 181 137 0
//orange: 203 75 22
//red 220 50 47
//magenta: 211 54 130
//violet: 108 113 196
//blue: 38 139 210
//cyan 42 161 152
//green 133 153 0
//======BASE EDITOR AND DISPLAY COLORS==========================================
//background for output window
#define BG1 "\x1b[48;2;0;43;54m"
//background for input window
#define BG2 "\x1b[48;2;238;232;213m"
//foreground for output window
#define FG1 "\x1b[38;2;147;161;161m"
//foreground for input window
#define FG2 "\x1b[38;2;131;148;150m"
//background for cursor
#define BGC "\x1b[48;2;253;246;227m"
//color for line numbers
#define LN "\x1b[38;2;42;161;m"
//======SYNTAX HIGHLIGHTING COLORS==============================================
//parentheses, "'", "[]", and "|"
#define TERMINATING "\x1b[38;2;133;153;0m"
//"\"
#define ESCAPE "\x1b[38;2;203;75;22;m"
#define STRINGS ""
#define SYMBOLS
/*
HL1 = "\x1b[38;2;211;54;138m",
//highlight 2 is for symbols and quotes
*HL2 = "\x1b[38;2;203;75;22m",
//highlight 3 is for function names
*HL3 = "\x1b[38;2;42;161;152m",
//highlight 4 is for parentheses
*HL4 = "\x1b[38;2;133;153;0m";
*/
//      ERROR STRINGS ==========================================================
const char
*ERR_end_of_file = {"End of File encountered too early!"},
*ERR_reader_error_invalid = {"The Reader encountered an invalid character!"},
*ERR_right_p = {"The Reader encountered an unmatched right parenthesis"};
/******************************************************************************/






////////////////////////////////////////////////////////////////////////////////
//	THE ZOOM VIRTUAL MACHINE
////////////////////////////////////////////////////////////////////////////////

//linked list node for the virtual memory page =================================
struct addr
{
	char c;
	struct addr *a, *b;
};
//virtual CPU registers ========================================================
long long int reg[16];
long double freg[8];

//head and tail of virtual memory page =========================================
struct addr *a, *z;

/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	VARIABLES FOR RUNNING THE REPL
////////////////////////////////////////////////////////////////////////////////

//string that represents the standard readtable ================================
//the last character represents the readtable case
//a = :upcase, b = :downcase, c = :preserve, d = :invert
const char* STANDARD_READTABLE = "       \a\b\t\n\v\f\r                   !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ a";
//the current readtable
const char *READTABLE;

int READ_DEFAULT_FLOAT_FORMAT;
int READ_SUPPRESS;
int READ_BASE = 10;

struct env;
struct package;
struct symbol;
struct object;
struct property;
struct sys_class
{
	//name of class
	const char *name;
	//array of inherited types
	const char **supertypes;
	//number of inherited types
	int supertypesN;
};
struct object
{
	struct sys_class *t;
	struct addr *data;
};
struct property
{
	struct object indicator, value;
};
struct symbol
{
	const char *name;
	struct package *pa;
	struct object *value;
	int function;
	struct property **pr;
	int propertyN;
	int shadowed;
	int t;
};
struct package
{
	const char *name;
	const char **nickname;
	int nicknameN;
	struct symbol **internal;
	struct symbol **external;
};
struct env
{
	int cPackage;
	struct package **p;
	int packageN;
	struct sys_class **c;
	int sys_classN;
};
struct env GLOBAL_ENV;
struct atom
{
	struct atom *p, *b;
	void *a;
	int t;
};

//function to execute the REPL
void RUN (const char *input);

//function to perform autocomplete suggestions
const char  *MATCH (const char *pattern);

//function that performs the read algorithm
void READ (struct atom *a, const char *s, int *pos, int *parens);
//reads a token (step 8 of the reader algorithm)
int getToken (const char *s, int *pos, const char *t);
//reads a token (step 9 of the reader algorithm)
int getTokenE (const char *s, int *pos, const char *t);
void EVAL (struct atom *a);
void errorOutput (const char *s);
void debugOutput (char *s);
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	THE EDITOR
////////////////////////////////////////////////////////////////////////////////

struct termios editor, preserve;
struct buffer
{
	char **b;
	int s;

};
struct buffer OBUFFER, IBUFFER, *previousInput, prevShellInput;
int previousInScroll, previousInSize;
char *shellInput;
int prevShellScroll;
int isInit, resized, overtype, shell, colored, cx, cy, shellC, inputH, outputH,
ISC, OSC;
struct winsize WIN;
void getWinSize ();
void setScreen ();
void handleKey ();
void softPush ();
void hardPush ();
void restore ();
void hardExit (const char *s);
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	ENTRY POINT (Main)
////////////////////////////////////////////////////////////////////////////////

int main (void)
{
	const char *TERM, **forms, *tmp, mode = '\0';
	int *termDir, *termFile, ret;
	FILE *f;
	struct termios editor;
	READTABLE = STANDARD_READTABLE;
	tcgetattr (STDIN_FILENO, &preserve);
	editor = preserve;
	editor.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	editor.c_oflag &= ~OCRNL;
	editor.c_oflag |= OPOST;
        editor.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
        editor.c_cflag |= CS8;
        editor.c_cc[VMIN] = 0;
        editor.c_cc[VTIME] = 0;
	if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &editor) == -1)
		hardExit ("tcsetattr failed");

	write (STDOUT_FILENO, SETC_I, strlen (SETC_I));

	isInit = 1;
	resized = 0;
	overtype = 1;
	shell = 1;
	colored = 0;
	cx = 1;
	cy = 1;
	shellC = 0;
	inputH = 0;
	outputH = 0;
	ISC = 0;
	OSC = 0;

	const struct timespec waitPeriod [] = {0, 1000000L};

	while (1)
	{
		getWinSize ();
		if (resized)
			setScreen ();
		handleKey ();
		nanosleep (waitPeriod, 0);
	}
	write (STDOUT_FILENO, "\x1b[0m", 4);
	write (STDOUT_FILENO, "\x1b[2J", 4);
	write (STDOUT_FILENO, "\x1b[H", 3);
	write (STDOUT_FILENO, SETC_V, 6);
	tcsetattr (STDOUT_FILENO, TCSAFLUSH, &preserve);
}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
void getWinSize ()
{
	int oldw = WIN.ws_col, oldh = WIN.ws_row;
	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &WIN) == -1 || WIN.ws_col == 0)
		hardExit ("Couldn't get window size!");
	else
	{
		if (oldw != WIN.ws_col || oldh != WIN.ws_row)
		{
			resized = 1;
			outputH = WIN.ws_row / 4;
			inputH = WIN.ws_row - outputH;
		}
		else
		{
			resized = 0;
		}
	}
}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	setScreen
////////////////////////////////////////////////////////////////////////////////
void setScreen ()
{
	int o = 0, nl = 0;
	char *tmp, *t;
	if (WIN.ws_col < 32 || WIN.ws_row < 4)
		hardExit ("Screen is too small!\nMinimum screen size is 4x32 characters.");
	if (isInit > 0)
	{
		OBUFFER.b = malloc (sizeof (char**) * 4);
		OBUFFER.b[0] = ";Welcome to Zoom!";
		OBUFFER.b[1] = ";Copyright (c) 2020";
		OBUFFER.b[2] = ";Daniel (Robin) Smith";
		OBUFFER.b[3] = ";Zoom conforms with the requirements of ANSI X3.226.";
		OBUFFER.s = 4;
		OBUFFER.s = 4;
		IBUFFER.b = malloc (sizeof (char[1]));
		IBUFFER.s = 1;
		isInit = 0;
	}
	if (shell)
	{
		tmp = malloc (strlen (NO_STYLE) + strlen (SETC_TOPL));
		strcat (tmp, NO_STYLE);
		strcat (tmp, SETC_TOPL);
		if (colored)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (BG1) + strlen (FG1));
			strcat (tmp, BG1);
			strcat (tmp, FG1);
		}
		for (int i = OSC; i < WIN.ws_row + OSC - o - 1; i ++)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (CLEAR_LINE));
			strcat (tmp, CLEAR_LINE);
			if (i < OBUFFER.s)
			{
				if (OBUFFER.b[i] != 0)
				{
					tmp = realloc (tmp, strlen (tmp) + strlen (OBUFFER.b[i] + 1));
					strncat (tmp, OBUFFER.b[i], strlen (OBUFFER.b[i]));
					strncat (tmp, "\n", 1);
					if (strlen (OBUFFER.b[i]) > WIN.ws_col)
					{
						o += strlen (OBUFFER.b[i]) / WIN.ws_col;
					}
				}
			}
			else
			{
				if (i < WIN.ws_row + OSC - o - 1)
				{
					tmp = realloc (tmp, strlen (tmp) + 1);
					strncat (tmp, "\n", 1);
				}
			}
			tmp = realloc (tmp, strlen (tmp) + strlen (CLEAR_LINE));
			strcat (tmp, CLEAR_LINE);
			if (shellInput != 0 && strlen (shellInput) > 0)
			{
				if (shellC < strlen (shellInput))
				{
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2));
						strncat (tmp, BG2, strlen (BG2));
						strncat (tmp, FG2, strlen (FG2));
					}
					tmp = realloc (tmp, strlen (tmp) + shellC);
					strncat (tmp, shellInput, shellC);
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (LN) + strlen (BGC));
						strncat (tmp, LN, strlen (LN));
						strncat (tmp, BGC, strlen (BGC));
					}
					tmp = realloc (tmp, strlen (tmp) + strlen (UNDERLINE) + strlen (NO_STYLE) + 1);
					strncat (tmp, UNDERLINE, strlen (UNDERLINE));
					strncat (tmp, &shellInput[shellC], 0);
					strncat (tmp, NO_STYLE, strlen (NO_STYLE));
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) +  strlen (BG2) +  strlen (FG2));
						strncat (tmp, BG2, strlen (BG2));
						strncat (tmp, FG2, strlen (FG2));
					}
					tmp = realloc (tmp, strlen (tmp) + strlen (shellInput) - shellC);
					strncat (tmp, &shellInput[shellC + 1], strlen (shellInput) - shellC);
				}
				else
				{
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2));
						strncat (tmp, BG2, strlen (BG2));
						strncat (tmp, FG2, strlen (FG2));
					}
					tmp = realloc (tmp, strlen (tmp) + strlen (shellInput));
					strncat (tmp, shellInput, strlen (shellInput));
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (LN) + strlen (BGC));
						strncat (tmp, LN, strlen (LN));
						strncat (tmp, BGC, strlen (BGC));
					}
					tmp = realloc (tmp, strlen (tmp) + strlen (UNDERLINE) + strlen (NO_STYLE));
					strncat (tmp, UNDERLINE, strlen (UNDERLINE));
					strncat (tmp, NO_STYLE, strlen (NO_STYLE));
					if (colored)
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (BG1) + strlen (FG1));
						strncat (tmp, BG1, strlen (BG1));
						strncat (tmp, FG1, strlen (FG1));
					}
				}
			}
			else
			{
				if (colored)
				{
					tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2) + strlen (LN));
					strncat (tmp, BG2, strlen (BG2));
					strncat (tmp, FG2, strlen (FG2));
					strncat (tmp, LN, strlen (LN));
				}
				tmp = realloc (tmp, strlen (tmp) + strlen (UNDERLINE) + strlen (NO_STYLE));
				strncat (tmp, UNDERLINE, strlen (UNDERLINE));
				strncat (tmp, NO_STYLE, strlen (NO_STYLE));
				if (colored)
				{
					tmp = realloc (tmp, strlen (tmp) + strlen (BG1) + strlen (FG1));
					strncat (tmp, BG1, strlen (BG1));
					strncat (tmp, FG1, strlen (FG1));
				}
				write (STDOUT_FILENO, tmp, strlen (tmp));
			}
		}
	}
	else
	{
		free (tmp);
		tmp = malloc (strlen (NO_STYLE) + strlen (SETC_TOPL));
		strncat (tmp, NO_STYLE, strlen (NO_STYLE));
		strncat (tmp, SETC_TOPL, strlen (SETC_TOPL));
		if (colored)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (BG1) + strlen (FG1));
			strcat (tmp, strcat(strcat (tmp, BG1), strcat(tmp, FG1)));
		}
		for (int i = OSC; i < outputH + OSC - o; i ++)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (CLEAR_LINE));
			strncat (tmp, CLEAR_LINE, strlen (CLEAR_LINE));
			if (i < OBUFFER.s)
			{
				tmp = realloc (tmp, strlen (tmp) + strlen (OBUFFER.b[i]) + 1);
				strcat (tmp, OBUFFER.b[i]);
				strncat (tmp, "\n", 1);
				if (strlen (OBUFFER.b[i]) / WIN.ws_col > 0)
					o += strlen (OBUFFER.b[i]) / WIN.ws_col;
			}
			else
			{
				if (i < outputH + OSC - o)
				{
					tmp = realloc (tmp, strlen (tmp) + 1);
					strncat (tmp, "\n", 1);
				}
			}
		}
		write (STDOUT_FILENO, tmp, strlen (tmp));
//======print the input window==================================================
//======make some variables for accounting for tab expansions===================
//off keeps track of how many characters the tabs expand the string to
		int off, len, i, isY;
		free (tmp);
		tmp = malloc (15);
		sprintf (t, "\x1b[%i;1H", outputH + 1);
		tmp = realloc (tmp, strlen (tmp) + strlen (t));
		strncat (tmp, t, strlen (t));
		if (colored)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2));
			strncat (tmp, BG2, strlen (BG2));
			strncat (tmp, FG2, strlen (FG2));
		}
		for (i = 0; i < inputH - 1; i ++)
		{
			tmp = realloc (tmp, strlen (tmp) + strlen (CLEAR_LINE) + 1);
			strncat (tmp, CLEAR_LINE, strlen (CLEAR_LINE));
			strncat (tmp, "\n", 0);
		}
		tmp = realloc (tmp, strlen (tmp) + 15);
		strncat (tmp, t, strlen (t));
		len = 0;
		i = ISC;
		for (int h = ISC; h < inputH + ISC - len; h ++)
		{
			tmp = realloc (tmp, strlen (CLEAR_SCREEN));
			strncat (tmp, CLEAR_SCREEN, strlen (CLEAR_SCREEN));
			if (cy == i)
				isY = 1;
			else
				isY = 0;
			if (i < IBUFFER.s)
			{
				off = 0;
				for (int j = 0; j < strlen (IBUFFER.b[i]); j ++)
				{
					if (IBUFFER.b[i][j] == '\t')
						off += ((j + off) % TABL);
				}
				len += (strlen (IBUFFER.b[i]) + off + 3) / WIN.ws_col;
				if (isY)
				{
					if (cx < strlen (IBUFFER.b[i]))
					{
						tmp = realloc (tmp, strlen (tmp) + cx);
						strncat (tmp, IBUFFER.b[i], cx);
						if (colored)
						{
							tmp = realloc (tmp, strlen (tmp) + strlen (LN) + strlen (BGC));
							strncat (tmp, LN, strlen (LN));
							strncat (tmp, BGC, strlen (BGC));
						}
						tmp = realloc (tmp, strlen (tmp) + strlen (UNDERLINE) + strlen (NO_STYLE) + 1);
						strncat (tmp, UNDERLINE, strlen (UNDERLINE));
						strncat (tmp, &IBUFFER.b[i][cx], 0);
						strncat (tmp, NO_STYLE, strlen (NO_STYLE));
						if (colored)
						{
							tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2));
							strncat (tmp, BG2, strlen (BG2));
							strncat (tmp, FG2, strlen (FG2));
						}
						tmp = realloc (tmp, strlen (tmp) + strlen (IBUFFER.b[i]) - cx);
						strncat (tmp, &IBUFFER.b[i][cx + 1], strlen (IBUFFER.b[i]) - cx);
					}
					else
					{
						tmp = realloc (tmp, strlen (tmp) + strlen (IBUFFER.b[i]));
						strcat (tmp, IBUFFER.b[i]);
						if (colored)
						{
							tmp = realloc (tmp, strlen (tmp) + strlen (LN) + strlen (BGC));
							strncat (tmp, LN, strlen (LN));
							strncat (tmp, BGC, strlen (BGC));
						}
						tmp = realloc (tmp, strlen (tmp) + strlen (UNDERLINE) + strlen (NO_STYLE) + 1);
						strncat (tmp, UNDERLINE, strlen (UNDERLINE));
						strncat (tmp, " \0", 0);
						strncat (tmp, NO_STYLE, strlen (NO_STYLE));
						if (colored)
						{
							tmp = realloc (tmp, strlen (tmp) + strlen (BG2) + strlen (FG2));
							strncat (tmp, BG2, strlen (BG2));
							strncat (tmp, FG2, strlen (FG2));
						}
					}
				}
				else
				{
					tmp = realloc (tmp, strlen (tmp) + strlen (IBUFFER.b[i]));
					strcat (tmp, IBUFFER.b[i]);
				}
				if (i < inputH + ISC - len - 1)
				{
					tmp = realloc (tmp, strlen (tmp) + 1);
					strncat (tmp, "\n", 1);
				}
			}
			else
				if (i < inputH + ISC - len - 1)
				{
					tmp = realloc (tmp, strlen (tmp) + 1);
					strncat (tmp, "\n", 1);
				}
			i ++;
		}
		write (STDOUT_FILENO, tmp, strlen (tmp));
	}
}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
// handleKey
////////////////////////////////////////////////////////////////////////////////
void handleKey ()
{
	int isKey = 0;
	char key[7] = {0, 0, 0, 0, 0, 0, 0};
	if (read (STDIN_FILENO, key, 7) == -1 && errno != EAGAIN)
		hardExit ("read error");
	if (key[0] == 0)
	{
		return;
	}
	char a[1], save, tmp;
	int err = 0;
	if (key[0] == 27)
	{
		switch (key[2])
		{
			//up arrow
			case 'A':
			{
				if (shell)
				{
					if (prevShellInput.s > 0)
					{
						if (prevShellScroll + 1 < prevShellInput.s)
							prevShellScroll ++;
						free (shellInput);
						shellInput = malloc (strlen (prevShellInput.b[prevShellScroll]));
						shellInput = prevShellInput.b[prevShellScroll];
					}
				}
				else
				{
					if (IBUFFER.s == 0)
						break;
					if (cy - ISC == 0)
					{
						if (ISC > 0)
							ISC --;
						if (cy > 0)
							cy --;
					}
					else
						if (cy > 0)
							cy --;
					if (cx > strlen (IBUFFER.b[cy]))
						cx = strlen (IBUFFER.b[cy]);
				}
				setScreen ();
				break;
			}
			//down arrow
			case 'B':
			{
				if (shell)
				{
					if (prevShellScroll > 0)
					{
						prevShellScroll --;
						if (prevShellScroll == 0)
						{
							free (shellInput);
							shellInput = 0;
							shellC = 0;
						}
						else
						{
							free (shellInput);
							shellInput = malloc (strlen (prevShellInput.b[prevShellScroll]));
							shellInput = prevShellInput.b[prevShellScroll];
						}
					}
				}
				else
				{
					if (IBUFFER.s == 0)
						break;
					if (cy - ISC == inputH - 1)
					{
						if (cy - ISC > 0)
							ISC ++;
						if (cy < IBUFFER.s - 1)
							cy ++;
					}
					else if (cy < IBUFFER.s - 1)
					{
						cy ++;
					}
					if (cx > strlen (IBUFFER.b[cy]))
					{
						cx = strlen (IBUFFER.b[cy]);
					}
				}
				setScreen ();
				break;
			}
			//right arrow key
			case 'C':
			{
				if (shell)
				{
					if (shellC < strlen (shellInput))
						shellC ++;
				}
				else
				{
					if (IBUFFER.s == 0)
						break;
					if (cy < IBUFFER.s)
					{
						if (cx < strlen (IBUFFER.b[cy]))
							cx ++;
					}
				}
				setScreen ();
				break;
			}
			//left arrow
			case 'D':
			{
				if (shell)
				{
					if (shellC > 0)
						shellC --;
				}
				else
				{
					if (cy < IBUFFER.s)
					{
						if (cx > 0)
						{
							cx --;
						}
					}
				}
				setScreen ();
				break;
			}
		}
		return;
	}
	switch (key[0])
	{
		case CTRL_Q:
		{
			restore ();
			break;
		}
		case CTRL_D:
		{
			if (shell)
			{
				restore ();
				break;
			}
			char *tmp;
			for (int i = 0; i < IBUFFER.s; i ++)
			{
				OBUFFER.s ++;
				OBUFFER.b = realloc (OBUFFER.b, (sizeof OBUFFER.b) * OBUFFER.s);
				OBUFFER.b[OBUFFER.s - 1] = realloc (OBUFFER.b[OBUFFER.s - 1], strlen (";Z \0"));
				OBUFFER.b[OBUFFER.s - 1] = ";Z \0";
				strcat (OBUFFER.b[OBUFFER.s - 1], IBUFFER.b[i]);
				OBUFFER.b[OBUFFER.s - 1] = IBUFFER.b[IBUFFER.s - 1];
				tmp = realloc (tmp, strlen (tmp) + strlen (IBUFFER.b[i]) + 1);
				strcat (tmp, IBUFFER.b[i]);
				strncat (tmp, "\n\0", 0);
			}
			RUN (tmp);
			previousInSize ++;
			previousInput = realloc (previousInput, previousInSize);
			previousInput[previousInSize - 1] = IBUFFER;
			free (IBUFFER.b);
			IBUFFER.s = 0;
			cx = 0;
			cy = 0;
			ISC = 0;
			if (OBUFFER.s > outputH)
			{
				OSC = OBUFFER.s - outputH;
			}
			setScreen ();
			break;
		}
		case CTRL_O:
		{
			overtype = !overtype;
			break;
		}
		case CTRL_C:
		{
			if (shell)
				break;
			char *tmp;
			for (int i = 0; i < IBUFFER.s; i ++)
			{
				OBUFFER.s ++;
				OBUFFER.b = realloc (OBUFFER.b, sizeof (OBUFFER.b) * OBUFFER.s);
				OBUFFER.b[OBUFFER.s - 1] = realloc (OBUFFER.b[OBUFFER.s - 1], strlen (";Z \0"));
				OBUFFER.b[OBUFFER.s - 1] = ";Z \0";
				strcat (OBUFFER.b[OBUFFER.s - 1], IBUFFER.b[i]);
				OBUFFER.b[OBUFFER.s - 1] = IBUFFER.b[IBUFFER.s - 1];
				tmp = realloc (tmp, strlen (tmp) + strlen (IBUFFER.b[i]) + 1);
				strcat (tmp, IBUFFER.b[i]);
				strncat (tmp, "\n\0", 0);
			}
			RUN(tmp);
			if (OBUFFER.s > outputH)
			{
				OSC = OBUFFER.s - outputH;
			}
			setScreen ();
			break;
		}
		case CTRL_N:
		{
			break;
		}
		case CTRL_W:
		{
			if (OBUFFER.s > WIN.ws_row)
			{
				OSC = OBUFFER.s - WIN.ws_row;
			}
			shell = !shell;
			setScreen ();
			break;
		}
		case CTRL_A:
		{
			colored = !colored;
			setScreen ();
			break;
		}
		case CTRL_K:
		{
			if (OSC < OBUFFER.s - 1)
			{
				OSC ++;
				setScreen ();
			}
			break;
		}
		case CTRL_L:
		{
			if (OSC > 0)
			{
				OSC --;
				setScreen ();
			}
			break;
		}
		case CTRL_Z:
		{
			if (previousInSize == 0)
				break;
			if (previousInScroll >= previousInSize)
				break;
			else
			{
				if (previousInScroll == 0)
				{
					previousInSize ++;
					previousInput = realloc (previousInput, sizeof (previousInput) * previousInSize);
					previousInput[previousInSize - 1] = IBUFFER;
					previousInScroll ++;
				}
				free (IBUFFER.b);
				IBUFFER.b = previousInput[previousInSize - previousInScroll - 1].b;
				IBUFFER.s = previousInput[previousInSize - previousInScroll - 1].s;
				if (previousInScroll < previousInSize)
					previousInScroll;
				cx = 0;
				cy = 0;
				ISC = 0;
				setScreen ();
			}
			break;
		}
		case CTRL_X:
		{
			if (previousInScroll > 0)
			{
				previousInScroll ++;
				free (IBUFFER.b);
				IBUFFER.b = previousInput[previousInSize - previousInScroll - 1].b;
				IBUFFER.s = previousInput[previousInSize - previousInScroll - 1].s;
				cx = 0;
				cy = 0;
				ISC = 0;
			}
		}
		case CTRL_E:
		{
			break;
		}
		case CTRL_S:
		{
			break;
		}
		case CTRL_R:
		{
			getWinSize ();
			setScreen ();
			break;
		}
		case 13:
		case 10:
		{
			if (shell)
			{
				prevShellInput.s ++;
				prevShellInput.b = realloc (prevShellInput.b, sizeof (prevShellInput.b) * prevShellInput.s);
				prevShellInput.b[prevShellInput.s - 1] = shellInput;
				shellInput = 0;
				prevShellScroll = 0;
				OBUFFER.s ++;
				OBUFFER.b = realloc (OBUFFER.b, sizeof (OBUFFER.b) * OBUFFER.s);
				strncat (OBUFFER.b[OBUFFER.s - 1], ";Z: ", sizeof (";Z: "));
				strcat (OBUFFER.b[OBUFFER.s - 1], shellInput);
				RUN (shellInput);
				shellC = 0;
				if (OBUFFER.s > WIN.ws_row)
				{
					OSC = OBUFFER.s - WIN.ws_row;
				}
			}
			else
			{
				previousInScroll = 0;
				if (IBUFFER.s == 0)
				{
					IBUFFER.s = realloc (IBUFFER.s, sizeof (IBUFFER.s) * 2);
					cy = 1;
					cx = 0;
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
//	restore
////////////////////////////////////////////////////////////////////////////////
void restore ()
{
	if (tcsetattr (STDIN_FILENO, TCSAFLUSH, &preserve) == -1)
		hardExit ("restore failed!");
	write (STDOUT_FILENO, "\x1b[0m", 4);
	write (STDOUT_FILENO, "\x1b[2J", 4);
	write (STDOUT_FILENO, "\x1b[H", 3);
	write (STDOUT_FILENO, SETC_V, sizeof (SETC_V));
	printf ("%s\n", "Thank you for using Zoom!");
	exit (0);
}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	RUN
////////////////////////////////////////////////////////////////////////////////
void RUN (const char *input)
{

}
/******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//	hardExit
////////////////////////////////////////////////////////////////////////////////
void hardExit (const char *s)
{
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &preserve);
	write (STDOUT_FILENO, "\x1b[0m", 4);
//clear the screen
        write(STDOUT_FILENO, "\x1b[2J", 4);
//move cursor to left
        write(STDOUT_FILENO, "\x1b[H", 3);
        write (STDOUT_FILENO, SETC_V, 6);
//print error and exit program
        perror (s);
        exit (-1);
}
