.TH common_lisp "lisp" "2020" "CLick Environment" "CLick ANSI Common Lisp Manual"
.SH NAME
ANSI Common Lisp
.SH DESCRIPTION
.PP
This is a manual for ANSI Common Lisp based on the ANSI X3.226 specification. This is not an official ANSI document.
.PP
The manual describes the 
.PP
ANSI Common Lisp is a high-level, multi-purpose, mostly functional programming language based on the mathematical language, Lambda Calculus.
.SH SEE ALSO
.IP
.BR syntax (lisp)
.BR lambda_list (lisp)
.BR loop (lisp)
.BR conditions (lisp)
.BR clos (lisp)
